/**
 *  utilisation de classes existantes
 *  @author Françoise GAYRAL
 */

import java.util.*;
public class TestUtilisationClasses
{
  public static void main(String[] args)	{
	// Pour l'exo 2 sur les chaînes : utilisation de la classe String
	String sb = "Bonjour tout le monde ";
	String m = new String("monde");
	String mb = "    monde        "; //4 blancs au départ, 8 blancs à la fin
	String me = "et merci";
	// à compléter  
	


	

	
   }	// fin du main
}// fin de la classe TestUtilisationClasses
